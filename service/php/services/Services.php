<?php


class Services
{
  public $mysqli;
  public $data ;
  public function __construct()
  {
    $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if ($this->mysqli->connect_errno) {
      return  "Fallo al conectar a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
    }
    return $this->mysqli;

  }

  public  function  getData ()
  {
    $generos ='SELECT * FROM generos';

    $departamentos = 'SELECT IdDepartamento, Departamento FROM departamentos ';


    $data= [];
    if ( $this->mysqli->multi_query($generos.';'.$departamentos)) {
      do {
        /* almacenar primer juego de resultados */
        if ($result = $this->mysqli->store_result()) {
          while ($row = $result->fetch_all()) {
            array_push($data, [
              $row
            ]);
          }
          $result->free();
        }


      } while ($this->mysqli->next_result());
    }

    return $data;

  }

  public function  getProvincia ($id) {
    $provincia = $this->mysqli->query('SELECT IdProvincia, Provincia FROM provincias WHERE IdDepartamento='.$id);
    if ($provincia->num_rows > 0 ) {
      return $provincia->fetch_all();
    }
  }
  public function  getDistrito ($id) {
    $provincia = $this->mysqli->query('SELECT IdDistrito, Distrito FROM distritos WHERE IdProvincia='.$id);
    if ($provincia->num_rows > 0 ) {
      return $provincia->fetch_all();
    }
  }

  public function saveRegister ($data) {
    return $data;
  }
}