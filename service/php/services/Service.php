<?php
//ini_set('max_execution_time', 6000);

require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once(INCDIR.'swiftmailer/swift_required.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once(INCDIR.'fpdf/fpdf.php');
class Service
{
    private $db;
    private $log;
    private $excels_dir;
    private $campoLink;
    private $pub;

    function __construct() 
    {




        $GLOBALS['amfphp']['encoding'] = 'amf3';
        $this->db = new ezSQL_mysql(DB_USER,DB_PASS,DB_NAME,DB_HOST);

        if(PRODUCTION_SERVER) $this->db->hide_errors();
        $this->excels_dir = PHPDIR.'../excels' ;


        $this->pub=false;
        if($this->pub){
            $this->campoLink = 'url1';
        }else{
            $this->campoLink = 'url2';
        }
    }    

    private function codificarPalabra($label){
        return mb_check_encoding ( $label ,  'UTF-8' )  ? $label : utf8_encode ( $label);
    }
    
    private function cerosIzquierda($n,$l){
        $retorno= str_pad($n, $l, "0", STR_PAD_LEFT);
        return $retorno;
    }
    private function guardarExcel( $objPHPExcel, $name )
    {
        $objPHPExcel->setActiveSheetIndex(0);       
        date_default_timezone_set('America/Lima');
        $fecha = new DateTime();
        $llave = $fecha->getTimestamp();
        $filename = $name . "_".$llave.".xls";        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $carpeta=$this->excels_dir."/".$fecha->format("Y-m-d");
        if(!file_exists($carpeta)){
            mkdir($carpeta);
        }
        $objWriter->save( $carpeta . "/" . $filename );    
        return $filename;
    }
    private function cabezerasExcel($title,$descripcion='',$keywords='',$category='')
    {        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("registro de horas de trabajo")
                                     ->setTitle($title)
                                     ->setSubject($title)
                                     ->setDescription($descripcion)
                                     ->setKeywords($keywords)
                                     ->setCategory($category);
        return $objPHPExcel;
    }
    private function sanear_string($string)
    {

        $string = trim($string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "°", "~",
                 "#", "@", "|", "!", "\"",
                 "·", "$", "%", "&", "/",
                 "(", ")", "?", "'", "¡",
                 "¿", "[", "^", "`", "]",
                 "+", "}", "{", "¨", "´",
                 ">", "< ", ";", ",", ":",
                  " "),
            '',
            $string
        );


        return $string;
    }
    /**
        CLASES PARA EL ADMINISTRADOR
    **/
    public function getGeneros()
    {
        $sql="SELECT * FROM generos";
        $res=$this->db->get_results($sql);
        if($res){
            return $res;
        }else{
            return 0;
        }
    }
    public function getDepartamentos()
    {
        $sql="SELECT IdDepartamento, Departamento FROM departamentos";
        $res=$this->db->get_results($sql);
        if($res){
            return $res;
        }else{
            return 0;
        }
    }
    public function getProvincias($data)
    {
        $sql="SELECT IdProvincia, Provincia FROM provincias WHERE IdDepartamento=$data";
        $res=$this->db->get_results($sql);
        if($res){
            return $res;
        }else{
            return 0;
        }
    }
    public function getDistritos($data)
    {
        $sql="SELECT IdDistrito, Distrito FROM distritos WHERE IdProvincia=$data";
        $res=$this->db->get_results($sql);
        if($res){
            return $res;
        }else{
            return 0;
        }
    }
    public function saveSuscripcion($data)
    {
        $data=json_decode($data);
        $app=$data->app;
        $apellido=$this->codificarPalabra($data->apellido);
        $nombre=$this->codificarPalabra($data->nombre);
        $fechaNacimiento=$data->fechaNacimiento;
        $dni=$data->dni;
        $departamento=$data->departamento;
        $provincia=$data->provincia;
        $distrito=$data->distrito;
        $telefono=$data->telefono;
        $correo=$data->correo;
        $genero=$data->genero;
        $ocupacion=$data->ocupacion;
        date_default_timezone_set('America/Lima');
        $fecha = date("Y-m-d H:i:s");
        $sqlPremiosSp="SELECT idPremio,descripcion,cantidad,muestraPremioEspecial,urlImagen FROM premios WHERE especial=1 AND programa=$app";
        $especiales=$this->db->get_results($sqlPremiosSp);
        $idPremio=$especiales[0]->idPremio;
        $descripcion=$especiales[0]->descripcion;
        $cantidad=$especiales[0]->cantidad;
        $muestraPremioEspecial=$especiales[0]->muestraPremioEspecial;
        $urlImagen=$especiales[0]->urlImagen;
        $sqlVerifica="SELECT count(idPremio) FROM detallepremiosespeciales WHERE idPremio=$idPremio";
        $cantidadVerifica=$this->db->get_var($sqlVerifica);
        if($cantidadVerifica<1){
            $ini=1;
            $fin=$ini*$muestraPremioEspecial;
            for($i=0;$i<$cantidad;$i++){
                $win=rand($ini, $fin);
                $ini=$fin+1;
                $fin=$fin+$muestraPremioEspecial;
                $ins="INSERT INTO detallepremiosespeciales (idPremio,registro) VALUES ($idPremio,$win)";
                $this->db->query($ins);
            }
        }

        $sqlPremios="SELECT idPremio,descripcion,urlImagen FROM premios WHERE especial=0 AND primeros=0 AND programa=$app";
        $premios=$this->db->get_results($sqlPremios);
        $cantidadPremios=count($premios);
        $arrIds=array();
        $arrTxts=array();
        $arrImg=array();
        for($x=0;$x<$cantidadPremios;$x++){
            $arrIds[]=$premios[$x]->idPremio;
            $arrTxts[]=$premios[$x]->descripcion;
            $arrImg[]=$premios[$x]->urlImagen;
        }
        $clave = array_rand($arrIds, 1);

        $sqlVerificaRegistro="SELECT idSuscrito,registro FROM suscritos ORDER BY idSuscrito DESC LIMIT 1";
        $register=$this->db->get_results($sqlVerificaRegistro);
        $i=$register[0]->idSuscrito;
        $registro=$register[0]->registro;
        if($registro==undefined){
            $registro==1;
        }else{
            $registro=$registro+1;
        }
        $SqlDetallePremios="SELECT idPremio,registro FROM detallepremiosespeciales WHERE idPremio=(SELECT idPremio FROM premios WHERE especial=1 AND programa=$app)";
        $detallePremios=$this->db->get_results($SqlDetallePremios);
        $state=false;
        for($m=0;$m<count($detallePremios);$m++){
            if($registro==$detallePremios[$m]->registro){
                $state=true;
                $idPremioUpdate=$detallePremios[$m]->idPremio;
            }
        }
        $sqlDni="SELECT count(idSuscrito) FROM suscritos WHERE dni='$dni'";
        $existe=$this->db->get_var($sqlDni);
        if($existe>0){
            return 2;
        }
        if($state){
            $sql="INSERT INTO suscritos (apellido,nombre,fechaNacimiento,dni,departamento,provincia,distrito,telefono,email,idGenero,idPremio,fregistro,registro,ocupacion) VALUES 
            ('$apellido','$nombre','$fechaNacimiento','$dni','$departamento','$provincia','$distrito','$telefono','$correo',$genero,$idPremio,'$fecha',$registro,'$ocupacion')";
            $premioFijo=$descripcion;
            $imgenFijo=$urlImagen;
            $sqlUpdate="UPDATE detallepremiosespeciales SET entregado=1 WHERE idPremio=$idPremioUpdate AND registro=$registro";
            //$this->correoConfirmacionPack($correo,$nombre);
            $this->db->query($sqlUpdate);
        }else{
            $sql="INSERT INTO suscritos (apellido,nombre,fechaNacimiento,dni,departamento,provincia,distrito,telefono,email,idGenero,idPremio,fregistro,registro,ocupacion) VALUES 
            ('$apellido','$nombre','$fechaNacimiento','$dni','$departamento','$provincia','$distrito','$telefono','$correo',$genero,$arrIds[$clave],'$fecha',$registro,'$ocupacion')";
            $premioFijo=$arrTxts[$clave];
            $imgenFijo=$arrImg[$clave];
        }
        $p=1;
        $res=$this->db->query($sql);

        $sqlPremiosPrimeros="SELECT idPremio,descripcion,cantidad,urlImagen FROM premios WHERE primeros=1 AND programa=$app";
        $primeros=$this->db->get_results($sqlPremiosPrimeros);
        $idPrimeros=$primeros[0]->idPremio;
        $descPrimeros=$primeros[0]->descripcion;
        $cantidadPrimeros=$primeros[0]->cantidad;
        $img3=$primeros[0]->urlImagen;
        $sqlPrimeros="SELECT idSuscrito,registro FROM suscritos ORDER BY idSuscrito DESC LIMIT 1";
        $reg=$this->db->get_results($sqlPrimeros);
        $idReg=$reg[0]->idSuscrito;
        $regReg=$reg[0]->registro;
        if($regReg <= $cantidadPrimeros){
            $p=$p+1;
            $sqlAdd="INSERT INTO tmp (premio,suscrito,fechaHora) VALUES ($idPrimeros,$idReg,'$fecha')";
            $this->db->query($sqlAdd);
        }
        $detalle = new stdClass();
        $detalle->cp1=$premioFijo;
        $detalle->cp2=$imgenFijo;
        if($p>1){
            $detalle->cp3=$descPrimeros;
            $detalle->cp4=$img3;
            if($premioFijo=='Wallpapers'){
                $this->correoConfirmacionSvrEntrada($correo,$nombre);
            }else if($premioFijo == 'Pack Andes'){
                $this->correoConfirmacionPackEntrada($correo,$nombre);
            }else{
                $this->correoConfirmacionGifEntrada($correo,$nombre);
            }
        }else{
            if($premioFijo=='Wallpapers'){
                $this->correoConfirmacionSvr($correo,$nombre);
            }else if($premioFijo == 'Pack Andes'){
                $this->correoConfirmacionPack($correo,$nombre);
            }else{
                $this->correoConfirmacionGif($correo,$nombre);
            }
        }
        if($res){
            return $detalle;
        }else{
            return 0;
        }
    }
    public function correoConfirmacionPack($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Has ganado 1 pack Andes Premium!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Eres uno de los 5 Celebrities Premium de Andes Films! Para ti, ¡tenemos los siguientes premios!</p>
                                <ul>
                                    <li>Una entrada doble para nuestra función especial de Pixeles</li>
                                    <li>Souvenirs de Andes Films</li>
                                    <li>Posters originales y merchandising de nuestros estrenos</li>
                                    <li>Free Pass Premium para nuestros Avant Premiere o Funciones especiales durante el 2015</li>
                                </ul>
                                <p>¡En la función especial a la que estás cordialmente invitado, te haremos entrega de este genial Pack Andes Premiun!</p>
                                <p>¡Hasta pronto!</p>
                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }
    public function correoConfirmacionPackEntrada($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Has ganado 1 pack Andes Premium!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Eres uno de los 5 Celebrities Premium de Andes Films! Para ti, ¡tenemos los siguientes premios!</p>
                                <ul>
                                    <li>Una entrada doble para nuestra función especial de Pixeles</li>
                                    <li>Souvenirs de Andes Films</li>
                                    <li>Posters originales y merchandising de nuestros estrenos</li>
                                    <li>Free Pass Premium para nuestros Avant Premiere o Funciones especiales durante el 2015</li>
                                </ul>
                                <p>¡En la función especial a la que estás cordialmente invitado, te haremos entrega de este genial Pack Andes Premiun!</p>
                                <p>¡Además!</p>
                                <p>Por estar dentro de los primeros 100 Usuarios Andes Premium, ¡estás cordialmente invitado a nuestra Función Especial de Pixeles!</p>
                                <p>En los próximos días te estará llegando a este correo la información necesaria a tomar en cuenta.</p>
                                <p><span style="font-size:10px;">Será necesaria la presentación de tu DNI para el ingreso a esta función.<span></p>
                                <p>¡Hasta pronto!</p>
                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }
    public function correoConfirmacionGifEntrada($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Gracias por registrarte en nuestra web!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Ahora que perteneces al Club Andes Premium podrás recibir adelantos exclusivos, material de nuestras próximas películas y participar en actividades especiales para ti!</p>
                                <p>Has ganado:</p>
                                <ul>
                                    <li>¡8 gifs exclusivos de tus películas favoritas!</li>
                                </ul>
                                <p>Descarga tu premio desde tu PC <a href="http://mediaimpact.pe/demo/andes/premios/Andes-Premium-Gifs.zip" target="_blank">aquí</a>.</p>
                                <p>¡Además!</p>
                                <p>Por estar dentro de los primeros 100 Usuarios Andes Premium, ¡estás cordialmente invitado a nuestra Función Especial de Pixeles!</p>
                                <p>En los próximos días te estará llegando a este correo la información necesaria a tomar en cuenta.</p>
                                <p><span style="font-size:10px;">Será necesaria la presentación de tu DNI para el ingreso a esta función.<span></p>
                                <p>¡Hasta pronto!</p>

                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }
    public function correoConfirmacionSvrEntrada($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Gracias por registrarte en nuestra web!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Ahora que perteneces al Club Andes Premium podrás recibir adelantos exclusivos, material de nuestras próximas películas y participar en actividades especiales para ti!</p>
                                <p>Has ganado:</p>
                                <ul>
                                    <li>¡3 Wallpapers exclusivos de tus películas favoritas!</li>
                                </ul>
                                <p>Descarga tu premio desde tu PC <a href="http://mediaimpact.pe/demo/andes/premios/Andes-Premium-svr.zip" target="_blank">aquí</a>.</p>
                                <p>¡Además!</p>
                                <p>Por estar dentro de los primeros 100 Usuarios Andes Premium, ¡estás cordialmente invitado a nuestra Función Especial de Pixeles!</p>
                                <p>En los próximos días te estará llegando a este correo la información necesaria a tomar en cuenta.</p>
                                <p><span style="font-size:10px;">Será necesaria la presentación de tu DNI para el ingreso a esta función.<span></p>
                                <p>¡Hasta pronto!</p>

                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }
    public function correoConfirmacionGif($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Gracias por registrarte en nuestra web!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Ahora que perteneces al Club Andes Premium podrás recibir adelantos exclusivos, material de nuestras próximas películas y participar en actividades especiales para ti!</p>
                                <p>Has ganado:</p>
                                <ul>
                                    <li>¡8 gifs exclusivos de tus películas favoritas!</li>
                                </ul>
                                <p>Descarga tu premio desde tu PC <a href="http://mediaimpact.pe/demo/andes/premios/Andes-Premium-Gifs.zip" target="_blank">aquí</a>.</p>

                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }
    public function correoConfirmacionSvr($email,$nombre)
    {
        date_default_timezone_set('America/Lima');
        $back='http://www.mediaimpact.pe/demo/andes/img/fondo-andes.png';
        $lpd_mail = "web";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Andes Films <$lpd_mail>\r\n";
        $headers .= "Reply-To: $lpd_mail\r\n";
        $message = '<html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Contacto</title>
                    </head>
                    <body style=" font-family: "courier";">
                        <div id="container" style="margin: 0 10%; width: 80%;">
                            <div id="imagen" style="text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-1.jpg" style="width:100%;">
                            </div>
                            <div id="contenido" style="border-radius: 5px; color: #000000; padding: 30px;">
                                <h2>¡Gracias por registrarte en nuestra web!</h2>
                                <p>Estimado(a) <span class="bold" style="font-weight: bold;">'.$nombre.'</span>.</p>
                                <p>¡Ahora que perteneces al Club Andes Premium podrás recibir adelantos exclusivos, material de nuestras próximas películas y participar en actividades especiales para ti!</p>
                                <p>Has ganado:</p>
                                <ul>
                                    <li>¡3 Wallpapers exclusivos de tus películas favoritas!</li>
                                </ul>
                                <p>Descarga tu premio desde tu PC <a href="http://mediaimpact.pe/demo/andes/premios/Andes-Premium-svr.zip" target="_blank">aquí</a>.</p>

                            </div>
                            <div id="footer" style="color: #000000; text-align: center;">
                                <img src="http://www.mediaimpact.pe/demo/andes/img/Banner_Andes-2.png" style="width:100%;">
                            </div>
                        </div>
                    </body>
                    </html>';
        $message=utf8_encode($message);
        $message=utf8_decode($message);
        mail($email,"Andes Films - Promo",$message,$headers);
    }

    public function emailPersonalizado($tipo,$email,$nombre)
    {
        if($tipo==1){
            $this->correoConfirmacionPack($email,$nombre);
        }else if($tipo==2){
            $this->correoConfirmacionSvr($email,$nombre);
        }else if($tipo==3){
            $this->correoConfirmacionGif($email,$nombre);
        }else if($tipo==4){
            $this->correoConfirmacionPackEntrada($email,$nombre);
        }else if($tipo==5){
            $this->correoConfirmacionSvrEntrada($email,$nombre);
        }else{
            $this->correoConfirmacionGifEntrada($email,$nombre);
        }
    }
    public function mailMasivo($cantidad)
    {
        $sql="SELECT email FROM suscritos ORDER BY idSuscrito ASC LIMIT $cantidad";
        $res=$this->db->get_results($sql);
        return $res;
        $exitosos=0;    
        $fallidos=0;
        for($i=0;$i<count($res);$i++){
            $email=$res[$i]->email;
            date_default_timezone_set('America/Lima');
            $lpd_mail = "web";
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: Andes Films <$lpd_mail>\r\n";
            $headers .= "Reply-To: $lpd_mail\r\n";
            $message = '<html>
                        <head>
                            <meta charset="UTF-8">
                            <title>Mailing Andes</title>
                        </head>
                        <body style=" font-family: "courier";">
                            <div id="container" style="margin: 0 10%; width: 80%;">
                                <div id="imagen" style="text-align: center;">
                                    <img src="http://www.mediaimpact.pe/demo/andes/img/logo-andes-premium.png" width="200px">
                                </div>
                                <div>
                                    <img src="http://www.mediaimpact.pe/demo/mailing.png" width="630px">
                                </div>
                                <div id="footer" style="background: #850909; color: #ffffff; text-align: center;">
                                    <p>Andes Films</p>
                                </div>
                            </div>
                        </body>
                        </html>';
            if(mail($email,"Andes Films - Promo",$message,$headers)){
                $exitosos=$exitosos+1;
                //$state->exitoso=$email;
            }else{
                $fallidos=$fallidos+1;
                //$state->fallido=$email;
            }
        }
        $resultado = new stdClass(); 
        $resultado->exitosos=$exitosos;
        $resultado->fallidos=$fallidos;
        //$resultado->resumen=$data;
        return $resultado;       
    }
}
?>

