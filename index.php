<!DOCTYPE html>
<html lang="es" id="app" data-program="1">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="datePicker/jquery.mobile.datepicker.css" />
	<link rel="stylesheet" href="datePicker/jquery.mobile.datepicker.theme.css" />
	<title>Andes Films</title>
</head>
<body>
	<div class="container" id="page1">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <figure>
                    <img src="img/logo-andes-premium.png" alt="">
                </figure>
            </div>
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
		<form class="form" id="formularioRegistro">
			<div class="form-group">
    			<label for="txtApellidos">Apellidos</label>
    			<input type="text" class="form-control"  name="apellidos" id="txtApellidos" placeholder="Apellidos">
  			</div>
  			<div class="form-group">
    			<label for="txtNombres">Nombres</label>
    			<input type="text" class="form-control" name="nombres" id="txtNombres" placeholder="Nombres">
  			</div>
  			<div class="form-group">
    			<label for="txtFecha">Fecha de nacimiento</label>
    			<input type="text" class="form-control date" name="fecha_nacimiento" id="txtFecha" placeholder="Fecha de nacimiento">
  			</div>
  			<div class="form-group">
    			<label for="txtDni">Dni</label>
    			<input type="text" class="form-control" name="dni" id="txtDni" placeholder="DNI" maxlength="8">
  			</div>
  			<!--div class="form-group">
    			<label for="txtDireccion">Dirección</label>
    			<input type="text" class="form-control" id="txtDireccion" placeholder="Dirección">
  			</div-->
        <div class="form-group">
          <label for="lstDepartamento">Departamento</label>
          <select id="lstDepartamento" class="form-control" name="departamento">

          </select>
        </div>
        <div class="form-group">
          <label for="lstProvincia">Provincia</label>
          <select id="lstProvincia" class="form-control" name="provincia">

          </select>
        </div>
        <div class="form-group">
          <label for="lstDistrito">Distrito</label>
          <select id="lstDistrito" class="form-control" name="distrito">

          </select>
        </div>
  			<div class="form-group">
    			<label for="txtTelefono">Teléfono</label>
    			<input type="number" class="form-control" id="txtTelefono" name="telefono" placeholder="Teléfono">
  			</div>
  			<div class="form-group">
    			<label for="txtCorreo">Correo</label>
    			<input type="mail" class="form-control" id="txtCorreo" name="correo" placeholder="Correo">
  			</div>
        <div class="form-group">
          <label for="txtCorreo">Ocupación</label>
          <input type="ocupacion" class="form-control"  name="ocupacion"  id="txtOcupacion" placeholder="Ocupación">
        </div>
  			<div class="form-group">
    			<label for="lstGenero">¿Qué género de película prefieres?</label>
    			<select id="lstGenero" class="form-control" name="genero">

    			</select>
  			</div>
  			<div class="form-group">
  				<button class="btn btn-primary" id="btnLimpiar" data-section="formularioRegistro">Limpiar</button>
    			<button class="btn btn-primary" type="submit" id="btnGrabar">Enviar</button>
  			</div>
		</form>
	</div>
	<div class="container" id="page2">
		<div class="form">
			<h1>¡MUCHAS GRACIAS POR REGISTRATE!</h1>
            <div class="row" id="detallePremio">
            	
            </div>
            
            <p style="margin-top: 50px;">Ahora que perteneces al club Andes Premium podrás recibir adelantos exclusivos y participar en actividades especiales para ti</p>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<!--	<script src="js/F.js"></script>
	<script src="js/app/control/Helper.js"></script>
	<script src="js/proceso/proceso.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="datePicker/datepicker.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/vistas/Suscripcion.js"></script>
    <script src="js/main.js"></script> -->
    <!--icon-star btnPremium-->
    <div class="modal fade" id="precargaMsj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                    
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">Por favor espere...</div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>

<script  src="js/premiun.js"></script>
</body>
</html>