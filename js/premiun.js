
document.addEventListener('DOMContentLoaded',  () => {
  
  const selectDepartamentos = document.getElementById('lstDepartamento')
  const selectGeneros= document.getElementById('lstGenero')
  const selectProvincia= document.getElementById('lstProvincia')
  const selectDistrito = document.getElementById( 'lstDistrito')
  const fragmentDistrito = document.createDocumentFragment()
const fragmentDepartamentos = document.createDocumentFragment()
const fragmentGeneros = document.createDocumentFragment()
  const fragmentProvincia = document.createDocumentFragment()
  
  const templateOptions = (id, text, fragment , message = '-- Selecciona --') => {
    if(id == 1) {
      let firstOption = document.createElement('option')
      firstOption.value = 0
      firstOption.textContent = message
      fragment.appendChild(firstOption)
    }
    let option = document.createElement('option')
    option.value = id
    option.textContent = text
  
    fragment.appendChild(option)
  }
  
  
  //get Departamento
  const URL_PROD = 'https://www.andesfilms.com.pe/iframe_premiun/andes/service/apipost.php'
  const URL_DEV = '/andes/service/apipost.php'
  axios.get(`${URL_DEV}?type=data`)
    .then (response => {
      response.data.forEach( (parentElement, index) =>{
        if (parentElement[0].length > 0 ) {
          parentElement[0].forEach((childElement, childIndex) => {
            
            if (index == 0) {
              templateOptions(childElement[0], childElement[1], fragmentGeneros)
            }
            else {
              templateOptions(childElement[0], childElement[1], fragmentDepartamentos)
            }
            
          })
        }
      
      })
      
      selectDepartamentos.appendChild(fragmentDepartamentos)
      selectGeneros.appendChild(fragmentGeneros)
    })
    .catch( error => {
      console.log(error)
    })
  
  
  // Registro de formulario
  const formularioRegistro = document.getElementById('formularioRegistro')
  
  formularioRegistro.addEventListener('submit', e => {
    e.preventDefault()
   const frm = new FormData(formularioRegistro)
    axios.post(`${URL_DEV}`,{
      data : frm,
    })
      .then( respose => {
        console.log(respose)
      })
      .catch( error => {
      
      })
  })

// Limpiar formulario
  
  const clearForm = document.getElementById('btnLimpiar')
  clearForm.addEventListener('click' , e => {
    e.preventDefault()
    formularioRegistro.reset()
  })
  
  
  selectDepartamentos.addEventListener('change' , async (e) => {
    let id = e.target.value
   let result = await axios.get(`${URL_DEV}?type=provincia&value=${id}`)
      .then( response => {
        if (response.status == 200 ) {
          response.data.forEach((element, index ) => {
            templateOptions(element[0], element[1], fragmentProvincia)
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
    selectProvincia.innerHTML = ''
    selectProvincia.appendChild(fragmentProvincia)
  })
  selectProvincia.addEventListener('change' , async (e) => {
    
    let id = e.target.value
    let result = await axios.get(`${URL_DEV}?type=distrito&value=${id}`)
      .then( response => {
        if (response.status == 200 ) {
          response.data.forEach((element, index ) => {
            templateOptions(element[0], element[1], fragmentDistrito)
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
    selectDistrito.innerHTML = ''
    selectDistrito.appendChild(fragmentDistrito)
  })
  
 
})