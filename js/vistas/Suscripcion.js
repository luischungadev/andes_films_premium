function Suscripcion()
{
	var elementos=['txtApellidos','txtNombres','txtFecha','txtDni','lstDepartamento','lstProvincia','lstDistrito','txtTelefono','txtCorreo','txtOcupacion','lstGenero'];
	this.iniciar=function()
	{

		$("#lstProvincia").attr('disabled','disabled');
		$("#lstDistrito").attr('disabled','disabled');

		$( "#lstDepartamento" )
		  .change(function () {
		    var str = "";
		    $( "#lstDepartamento option:selected" ).each(function() {
		      str = $( this ).val();
			  proceso.procesar('getProvincias',str,listarProvincias);
			  $("#lstProvincia").removeAttr('disabled');
		    });

		  })
		  .change();

		$( "#lstProvincia" )
		  .change(function () {
		    var str2 = "";
		    $( "#lstProvincia option:selected" ).each(function() {
		      str2 = $( this ).val();
			  proceso.procesar('getDistritos',str2,listarDistritos);
			  $("#lstDistrito").removeAttr('disabled');
		    });

		  })
		  .change();

		proceso.procesar('getGeneros','',listarGeneros);
		proceso.procesar('getDepartamentos','',listarDepartamentos);
		$("#btnLimpiar").on('click',helper.limpiarCampos);
		$("#btnGrabar").on('click',grabar);
	}
	function listarGeneros(data)
	{
		$("#lstGenero").append('<option value="">Elige tu género favorito</option>');
		$.each(data, function( index, value ) {
		  	$("#lstGenero").append('<option value="'+value.idGenero+'">'+value.descripcion+'</option>');
		});
	}
	function listarDepartamentos(data)
	{
		$("#lstDepartamento").append('<option value="">Elige tu departamento</option>');
		$.each(data, function( index, value ) {
		  	$("#lstDepartamento").append('<option value="'+value.IdDepartamento+'">'+value.Departamento+'</option>');
		});
	}
	function listarProvincias(data)
	{
		$("#lstProvincia").append('<option value="">Elige tu Provincia</option>');
		$.each(data, function( index, value ) {
		  	$("#lstProvincia").append('<option value="'+value.IdProvincia+'">'+value.Provincia+'</option>');
		});
	}
	function listarDistritos(data)
	{
		$("#lstDistrito").append('<option value="">Elige tu Distrito</option>');
		$.each(data, function( index, value ) {
		  	$("#lstDistrito").append('<option value="'+value.IdDistrito+'">'+value.Distrito+'</option>');
		});
	}
	function grabar()
	{
		validado=helper.valida(elementos);
		if(validado==0){
			return;
		}
		var objSuscripcion=new Object();
		objSuscripcion.apellido=$("#txtApellidos").val();
		objSuscripcion.nombre=$("#txtNombres").val();
		objSuscripcion.fechaNacimiento=$("#txtFecha").val();
		objSuscripcion.dni=$("#txtDni").val();
		objSuscripcion.departamento=$("#lstDepartamento").val();
		objSuscripcion.provincia=$("#lstProvincia").val();
		objSuscripcion.distrito=$("#lstDistrito").val();
		objSuscripcion.telefono=$("#txtTelefono").val();
		objSuscripcion.correo=$("#txtCorreo").val();
		objSuscripcion.ocupacion=$("#txtOcupacion").val();
		objSuscripcion.genero=$("#lstGenero").val();
		objSuscripcion.app=$("#app").attr('data-program');
		proceso.procesar('saveSuscripcion',objSuscripcion,postSuscripcion);	
		$("#precargaMsj").modal('show');
	}
	function postSuscripcion(data)
	{
		$("#precargaMsj").modal('hide');
		if(data==2){
			alert('Ya está registrado con este número de DNI');
			return;
		}
		//console.log(data);

		$("#page1").hide();
		$("#page2").show();
		premio1=data.cp1;
		img1=data.cp2;
		premio2=data.cp3;
		img2=data.cp4;

		if(img2==""){
			imagen=img1+".png";
		}else{
			imagen=img1+"-"+img2+".png";
		}

		$("#detallePremio").append('<div class="col-md-12"><img src="img/'+imagen+'" alt="'+imagen+'" class="img-premio"></div>')
		helper.limpiarCampos2('formularioRegistro');

		$(".img-premio").css('width','70%');
	}
}