app.controller 'menuMainCrtl',['$scope','$location','$timeout','$http', (scope,location2,timeout,http) ->
	scope.detailmenu = []
	scope.detailmenu = {}
	scope.categoria = ""
	scope.id = 0
	scope.irSeccion = (id,link) ->
		window.top.location = link
		return
	scope.animationmain = ()->
		if $('.wrapper-menu').hasClass('active') == false
			$('.wrapper-menu').addClass('active')
		else
			$('.wrapper-menu').removeClass('active')
		return
	scope.viewMain = (id)->
		if (id!=0)
			data = {
				id : id
			}
			url = './controllers/menu/listar.php'
			result = http.post(url,data)
			result.success (response) ->
				scope.detailmenu = response.listar
				scope.id = response.id
				console.log scope.detailmenu
				scope.categoria = response.categoria
				timeout(scope.animationmain,100)
				return
			result.error (error) ->
				console.log(error)
				return
		else
			timeout(scope.animationmain,100)
		return
	return
]
