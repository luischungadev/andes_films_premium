app.config(['$facebookProvider', function($facebookProvider) {
    $facebookProvider.setAppId('1832334340329006').setPermissions(['email','user_friends','public_profile']);
}]);

app.run(['$rootScope', '$window', function($rootScope, $window) {
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  $rootScope.$on('fb.load', function() {
    $window.dispatchEvent(new Event('fb.load'));
  });
}]);

