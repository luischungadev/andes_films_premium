<?php
$miconexion = db_connect('','','','');
$sql 	= "Select * From usuario Group by idfacebook";
$rsUser 	= mysql_query($sql,$miconexion);
$nUser 	= mysql_num_rows($rsUser);
$html = '';
for($n = 0; $n < $nUser; $n++){
  $first_name      = mysql_result($rsUser,$n,"first_name");
  $last_name      = mysql_result($rsUser,$n,"last_name");
  $email    = mysql_result($rsUser,$n,"email");
  $gender   = mysql_result($rsUser,$n,"gender");

	$html.= '<tr>';
	$html.= '<td>';
	$html.= utf8_encode($first_name);
	$html.= '</td>';
	$html.= '<td>';
	$html.= utf8_encode($last_name);
	$html.= '</td>';
	$html.= '<td>';
	$html.= $email;
	$html.= '</td>';
	$html.= '<td>';
	$html.= $gender;
	$html.= '</td>';
	$html.= '</tr>';

}

function db_connect($server,$user,$pass,$database) {
    $result = mysql_connect($server, $user, $pass);
    if (!$result)
        return false;
    if (!mysql_select_db($database))
        return false;
    return $result;
}
?>

<!DOCTYPE html>
<html lang="es" ng-app="Peru2021">
<head>
<title>Ganadores Andes</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="index, follow">
<link rel="shortcut icon" href="public/image/favicon.ico" type="image/x-icon">
</head>
<style>
	
      /*

RESPONSTABLE 2.0 by jordyvanraaij
  Designed mobile first!

If you like this solution, you might also want to check out the 1.0 version:
  https://gist.github.com/jordyvanraaij/9069194

*/
.responstable {
  margin: 1em 0;
  width: 100%;
  overflow: hidden;
  background: #FFF;
  color: #024457;
  border-radius: 10px;
  border: 1px solid #167F92;
}
.responstable tr {
  border: 1px solid #D9E4E6;
}
.responstable tr:nth-child(odd) {
  background-color: #EAF3F3;
}
.responstable th {
  display: none;
  border: 1px solid #FFF;
  background-color: #167F92;
  color: #FFF;
  padding: 1em;
}
.responstable th:first-child {
  display: table-cell;
  text-align: center;
}
.responstable th:nth-child(2) {
  display: table-cell;
}
.responstable th:nth-child(2) span {
  display: none;
}
.responstable th:nth-child(2):after {
  content: attr(data-th);
}
@media (min-width: 480px) {
  .responstable th:nth-child(2) span {
    display: block;
  }
  .responstable th:nth-child(2):after {
    display: none;
  }
}
.responstable td {
  display: block;
  word-wrap: break-word;
  max-width: 7em;
}
.responstable td:first-child {
  display: table-cell;
  text-align: center;
  border-right: 1px solid #D9E4E6;
}
@media (min-width: 480px) {
  .responstable td {
    border: 1px solid #D9E4E6;
  }
}
.responstable th, .responstable td {
  text-align: left;
  margin: .5em 1em;
}
@media (min-width: 480px) {
  .responstable th, .responstable td {
    display: table-cell;
    padding: 1em;
  }
}

body {
  padding: 0 2em;
  font-family: Arial, sans-serif;
  color: #024457;
  background: #f2f2f2;
}

h1 {
  font-family: Verdana;
  font-weight: normal;
  color: #024457;
}
h1 span {
  color: #167F92;
}
</style>
<body>
<h1>TOTAL DE PARTICIPANTES: <strong><?php echo $nUser; ?></strong></h1>
<table class="responstable">
	<thead>
		<tr>
			<th>
				Nombre
			</th>
			<th>
				Apellido
			</th>
			<th>
				Email
			</th>
			<th>
				Género
			</th>
		</tr>
	</thead>
	<tbody>
		<?php echo $html; ?>
	</tbody>
</table>
</body>
</html>